<?php 
use PHPUnit\Framework\TestCase;

include_once(__DIR__."/../models/User.php");
include_once(__DIR__."/../models/Database.php");

final class SeanceTest extends TestCase {
    public function testCreateSeance()
    {
        $seance =Seance::createSeance("Pilates","Ce Cours détend", "09:00", date("Y-m-d"),50,20, "#03bafc");
                             
        $database = new Database();
        
        $this->assertNotFalse($database->createSeance($seance));
    }

    public function testGetSeanceById(){
        $database = new Database();

        $seance =Seance::createSeance("Pilates","Ce Cours détend", "09:00", date("Y-m-d"),50,20, "#03bafc");

        // je recupère l'id de la séance
        $id = $database->createSeance($seance);

        $this->assertInstanceOf(Seance::class, $database->getSeanceById($id));
    }

    public function testDeleteSeance(){
        $database = new Database();

        $seance =Seance::createSeance("Pilates","Ce Cours détend", "09:00", date("Y-m-d"),50,20, "#03bafc");

        // je recupère l'id de la séance
        $id = $database->createSeance($seance);

        $this->assertTrue($database->deleteSeance($id));
    }

    public function testGetSeanceByWeek(){
        $database = new Database();

        $seance =Seance::createSeance("Pilates","Ce Cours détend", "09:00", date("Y-m-d"),50,20, "#03bafc");
        var_dump($seance);
        $this->assertNotFalse($database->createSeance($seance));
        // je recupère l'id de la séance
        $nbSeances = count($database->getSeanceByWeek(date("W"))) ;

        echo ($nbSeances);
        var_dump($nbSeances);
        $this->assertGreaterThan(0, $nbSeances);
 
    }


}
