<?php
    //ceci remplace l'instruction include quand on défini un namespace à la classe
    use PHPUnit\Framework\TestCase;

    include(__DIR__ ."/../models/User.php");
    include(__DIR__ ."/../models/Seance.php");
    include(__DIR__ ."/../models/Database.php");

    final class UserTest extends TestCase
    {
   

        public function testEmailAlreadyExists()
        {
            $database = new Database();

            //Créer le user
            $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT),
                                    0, 0, bin2hex(random_bytes(20)));

            //L'insérer puis vérifier que tout s'est bien passé
            $this->assertNotFalse($database->createUser($user));
            
            //Vérifier un email qui existe
            $emailTrue = "toto@gmail.com";
            $this->assertTrue($database->isEmailExists($emailTrue));

            //Vérifier un email qui n'existe pas
            $emailFalse = "toto@hotmail.com";
            $this->assertFalse($database->isEmailExists($emailFalse));
        }

   



    }
?>