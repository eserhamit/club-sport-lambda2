<?php 
// ce fichier sert à les données du formulaire de login

// on va utiliser la session pour passer des messages d'une page à l'autre
// Pour cela il faut démarrer la session au début des pages concernées
session_start();

require_once (__DIR__."/../models/Database.php");
$database = new Database();

// On recupère les données du formulaire 
$email = isset($_POST["email"]) ? $_POST["email"] : null;
$password = isset($_POST["password"]) ? $_POST["password"] : null;
 
// Verifications
if($email== null || $password == null){ 
    $_SESSION["error"] = "L'email et le mot de passe sont obligatoires";
 header ("location: ../vues/login.php");
    exit();
}

// On recupère le user en BD 
$user = $database->getUserByEmail($email);
// On verifie que user a bien été retrouvé
if(!$user){ 
    $_SESSION["error"] = "L'email est incorrect, vous n'aves pas été trouvé";
  header ("location: ../vues/login.php");
    exit();
} 

// On verifie que le user est actif 
if($user->isActif()== 0){ 
    $_SESSION["error"] = "Votre compte n'a pas encore été validé, consultez vos emails";
  header ("location: ../vues/login.php");
    exit();
}

// On verifie le mot de passe 
if(!password_verify($password, $user->getPassword())){ 
    $_SESSION["error"] = "Le mot de passe est incorrect";
 header ("location: ../vues/login.php");
    exit();
}

// Tous les tests ont réussi donc on peut logguer le user
// On ajoute son id et l'objet user dans la session
$_SESSION["id"] = $user->getId();
$_SESSION["user"] = serialize($user);

// On redirige vers la page planing
$_SESSION["info"] = "Vous êtes bien connecté";
header ("location: ../vues/planing.php");
 
?>