<?php 
// Ce fichier sert à suprimer une séance

// On va utiliser la session pour passer des messages d'une page à l'autre 
// pour cela il faut démarrer la session au début des pages concernées
session_start();
require_once(__DIR__."/../models/Database.php");
$database = new Database();

// Récuperation de l'id dans l'url
$idSeance =$_GET["id"];

// Au cas où, verifions que nous avons bien un id
if(!$idSeance){
    //Si nous n'en avons pas il faut revenir à page précédente avec un message d'erreur
    $_SESSION["error"] = "Le lien de supression n'est pas correct";
    header ("location: ../vues/planning.php");

} 
// Si tout vas bien on suprime la séance
if($database->deleteSeance($idSeance)){
    // La séance a bien été supprimée
    $_SESSION["error"] = "Séance supprimée avec succès";
    header ("location: ../vues/planning.php".$id);
} else {
    // La séance n'a pas pu être supprimée
    $_SESSION["error"] = "Le lien de suppression n'est pas correct";
    header ("location: ../vues/planning.php".$id);
}
