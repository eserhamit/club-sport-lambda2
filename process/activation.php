<?php 
// ce fichier sert à activer un utilisateur qui vient de s'inscrire

// on va utiliser la session pour passer des messages d'une page à l'autre
// Pour cela il faut démarrer la session au début des pages concernées
session_start();

require_once (__DIR__."/../models/Database.php");
$database = new Database();

// On recupère les données dans l'url
$idUser = isset($_GET["id"]) ? $_GET["id"] : null;
$token = isset($_GET["token"]) ? $_GET["token"] : null;

// On fait la verification d'usage
if($idUser== null  || $token == null){ 
    $_SESSION["error"] = "Une erreur s'est produite lors de votre inscription, veuillez recommencer.";
 header ("location: ../vues/inscription.php");
    exit();
}

// On cherche l'utilisateur dans la BD grâce à son id
$user = $database->getUserById($idUser);
// On verifie que user a bien été retrouvé

if(!$user){ 
    $_SESSION["error"] = "Un problème est survenu lors de votre inscription, veuillez recommencer.";
 header ("location: ../vues/inscription.php");
    exit();
}

// On compare les token pour authentifier l'utilisateur 
if($token != $user->getToken()){ 
    $_SESSION["error"] = "Un problème est survenu lors de votre inscription, veuillez recommencer.";
 header ("location: ../vues/inscription.php");
    exit();
}

// si tout c'est bien passé on active l'utilisateur
if($database->activateUser($idUser)){ 
    // Puis on redirige vers la page de login avec un message de succès
    $_SESSION["info"] = "Votre compte a été activé, vous pouvez vous connecter.";
 header ("location: ../vues/inscription.php");
    exit();
}else {
    //Si l'activation a echoué on renvoi vers la page inscription
    $_SESSION["error"] = "Un problème est survenu lors de activation de votre compte, veuillez recommencer.";
 header ("location: ../vues/inscription.php");
}


?>