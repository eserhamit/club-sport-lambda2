<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Activer Votre compte Lambda</title>
  </head>
  <body>
    <div class="container">
        <h3 class="pt-5">Bonjour <?php echo $nom; ?></h3>
        <p>Bienvenue au club Lambda</p>
        <p>Vouz venez de vous inscrire. Pour activer votre compte, merci de cliquer sur le bouton d'activation 
        ci-dessous</p>
        <a href="http://localhost/process/activation.php?id=<?php echo $idUser;?>&token=<?php echo $token; ?>"
         class="btn btn-success">Activer mon compte</a>
    </div>
 
  </body>
</html>