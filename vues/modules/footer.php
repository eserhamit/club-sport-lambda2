<footer>
    <div class="container-fluid bg info text-white bt-2">
        <div class="row text-center text-xs-center text-sm-left text-md-left">
            <div class="col-xs-12 col-sm4 col-md-4">
                <h5>Quick Links</h5>
                <ul class="list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Get Started</a></li>
                    <li><a href="#">Videos</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm4 col-md-4">
                <h5>Quick Links</h5>
                <ul class="list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Get Started</a></li>
                    <li><a href="#">Videos</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm4 col-md-4">
                <h5>Quick Links</h5>
                <ul class="list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Get Started</a></li> 
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center badge badge-dark">
                <p class="h6">&copy All right Reserved. <a href="https://www.realise.ch" target="_blank" class="ml-2">Réalise</a></p>
            </div>
            <hr>
        </div>
    </div>
</footer>