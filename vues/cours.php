<?php include('modules/partie1.php'); ?>

<?php
//Page 70
// J'intancie la nouvelle Database
require_once("../models/Database.php");
// On instancie un nouvel objet Database ce qui va créer une connexion à la BD
$database = new Database();

//Je récupère l'id dans le url
// Si pas d'id dans l'url on prendra 1 par défaut 
$idSeance = isset($_GET["id"]) ? $_GET["id"] : 1;
// On veut récupérer tous les Babysitters

// Je vais chercher la séances dans la base de données
$seance = $database->getSeanceById($idSeance);

// Déserialisation du user pour savoir s'il est admin ou pas 
$user = unserialize($_SESSION["user"]);

// Récuperation du nombre d'inscrit
$nbInscrits = $database->nombreInscrits($idSeance);
//Est-ce que le user est déjà inscrit ou pas
$isInscrit = $database->isInscrit($user->getId(), $idSeance); 
?>

<div class="container card text-center mt-4">
    <h1 class="card-header"><?php echo $seance->getTitre(); ?></h1>
    <div class="card-body text-left" style="background: <?php echo $seance->getCouleur(); ?>">
        <div class="row">
            <div class="offset-3 col-9">
                <p>Date: <?php echo $seance->getDate(); ?></p>
            </div>
            <div class="offset-3 col-9">
                <p>Heure de début: <?php echo $seance->getHeureDebut(); ?></p>
            </div>
            <div class="offset-3 col-9">
                <p>Durée: <?php echo $seance->getDuree(); ?> Minutes</p>
            </div>
            <div class="offset-3 col-9">
                <p>Description</p>
            </div>
            <div class="offset-3 col-9">
                <p><?php echo $seance->getDescription(); ?></p>
            </div>
            <div class="offset-3 col-9">
                <p>Nombre de participants max : <?php echo $seance->getNbParticipantsMax(); ?></p>
            </div>
            <div class="col-12">
                <div class="d-flex justify-content-around m-2">
                    <?php if($isInscrit) { ?>
                    <a href="../process/desinscription-seance.php?id=<?php echo $seance->getId(); ?>" class="btn btn-danger">
                    Se désinscrire</a>
                    <?php } else if($nbInscrits<$seance->getNbParticipantsMax()) { ?>
                    <a href="../process/inscription-seance.php?id=<?php echo $seance->getId(); ?>" class="btn btn-primary">S'inscrire</a>
                    <?php } else { ?>
                    <a href="#" class="btn btn-danger">Complet</a>
                    <?php } ?>
                </div>
            </div>
            <?php if ($user->isAdmin() == 1) {  ?>
                <div class="col-12">
                    <div class="d-flex justify-content-around m-2">
                        <a href="../vues/formulaire.php?id=<?php echo $seance->getId(); ?>&type=3" class="btn btn-danger">Dupliquer</a>
                        <a href="../vues/formulaire.php?id=<?php echo $seance->getId(); ?>&type=2" class="btn btn-primary">Modifier</a>
                        <a href="../process/delete-seance.php?id=<?php echo $seance->getId(); ?>" class="btn btn-danger">Suprimer</a>
                    </div>
                </div>
            <?php } // endif 
            ?>
        </div>

    </div>
</div>
</div>

<?php include('modules/partie3.php'); ?>