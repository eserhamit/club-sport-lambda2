<?php
// On importe la classe user 
require_once(__DIR__ . "/../models/User.php");
// On recupère le user dans la session
$user = isset($_SESSION["user"]) ? unserialize($_SESSION["user"]) : null;
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php">Club Lambda</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
    <?php if ($user == null) { ?>
      <li class="nav-item active">
        <a class="nav-link" href="login.php">Login </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="inscription.php">Inscription</a>
      </li>
     <?php } //endif ?>
     <li class="nav-item">
        <a class="nav-link" href="planing.php">Planing</a>
      </li>
      <?php if ($user != null) {  ?>
        <?php if ($user->isAdmin() == 1) {  ?>
      
          <li class="nav-item">
            <a class="nav-link" href="formulaire.php?type=1">Créer cours</a>
          </li>
        <?php } // endif 
        ?>
        <li class="nav-item">
          <a class="nav-link" href="profil.php">Profil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../process/deconnexion.php">Déconnexion</a>
        </li>
      <?php } // endif 
      ?>

    </ul>
  </div>
</nav>