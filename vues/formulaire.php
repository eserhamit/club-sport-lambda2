<?php include('modules/partie1.php'); 
 
?>


<?php require_once(__DIR__."/../models/Database.php"); 
//Declarations de constantes pour les 3 cas
const CREATION =1;
const MODIFICATION =2;
const DUPLICATION =3;

// recuperation des valeur dans l'url 
$idSeanceOrigine = $_GET["id"];
$type = $_GET["type"];

// Recuperation de la séance d'origine si elle existe
if(isset($idSeanceOrigine)){
    $database = new Database();
    $seanceOrigine = $database-> getSeanceById($idSeanceOrigine);
}  
// Creation d'une séance pour remplir les valeur des inputa
$seance =new Seance();
$titre = ""; 


switch ($type) {
    case CREATION : 
        // on garde la séance vide
    break;
    case MODIFICATION :
        // on modifie la séance d'origine
        $seance = $seanceOrigine ;
        $titre = "Modification de ";
    break; 
    case DUPLICATION : 
        // on reprend tous les attibuts sauf l'id 
        $seance->setTitre($seanceOrigine->getTitre());
        $seance->setCouleur($seanceOrigine->getCouleur());
        $seance->setDate($seanceOrigine->getDate());
        $seance->setHeureDebut($seanceOrigine->getHeureDebut());
        $seance->setDuree($seanceOrigine->getDuree());
        $seance->setDescription($seanceOrigine->getDescription());
        $seance->setNbParticipantsMax($seanceOrigine->getNbParticipantsMax());
        $titre = "Duplication de ";
    break;
    default:
    // Par défaut on garde la séance vide pour effectuer une création 
    $titre = "Création d'une séance";
}

?> 

<div class="container card text-center mt-4">
    <h1 class="card-header"><?php echo  $titre.$seance->getTitre();?></h1>
    <div class="card-body text-left" style="background: #03bafc">
        <form action="../process/formulaire.php" class="text-left text-md-right" method="POST">
<input type="hidden" name="type" value="<?php echo $type; ?>" >
<input type="hidden" name="id" value="<?php echo $seance->getId(); ?>" >
            <div class="form-group row">
                <label for="nom" class="col-sm-12 col-md-4 col-form-label">Nom</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="titre" name="titre" placeholder="Nom du cours" value="<?php echo $seance->getTitre(); ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="Couleur" class="col-sm-12 col-md-4 col-form-label">Couleur de fond</label>
                <div class="col-sm-12 col-md-8">
                    <input type="color" class="form-control" id="couleur" name="couleur" value="<?php echo $seance->getCouleur(); ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="date" class="col-sm-12 col-md-4 col-form-label">Date</label>
                <div class="col-sm-12 col-md-8">
                    <input type="date" class="form-control" id="date" name="date" value="<?php echo $seance->getDate(); ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="heureDebut" class="col-sm-12 col-md-4 col-form-label">Heure de débout</label>
                <div class="col-sm-12 col-md-8">
                    <input type="time" class="form-control" id="heureDebut" name="heureDebut" value="<?php echo $seance->getHeureDebut(); ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="duree" class="col-sm-12 col-md-4 col-form-label">Durée</label>
                <div class="col-sm-12 col-md-8">
                    <input type="number" class="form-control" id="duree" name="duree" value="<?php echo $seance->getDuree(); ?>" required><span>Minutes</span>
                </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-sm-12 col-md-4 col-form-label">Description</label>
                <div class="col-sm-12 col-md-8">
                   <textarea class="form-control" name="description" id="description" cols="30" rows="10">
                   <?php echo $seance->getDescription(); ?>
                   </textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="nbParticipants" class="col-sm-12 col-md-4 col-form-label">Nombre de participants max</label>
                <div class="col-sm-12 col-md-8">
                    <input type="number" class="form-control" id="nbParticipants" name="nbParticipants" value="<?php echo $seance->getNbParticipantsMax(); ?>" required><span>Participants</span>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-dark" type="submit">
                <?php 
                switch($type){
                    case CREATION   : echo "Créer"; break;
                    case MODIFICATION : echo "Modifier"; break;
                    case DUPLICATION : echo "Dupliquer"; break;
                }
                ?>
                </button>
            </div>
        </form>
    </div>
</div>

<?php include('modules/partie3.php'); ?>