<?php include('modules/partie1.php'); ?>
 <div class="container ">
     <div class="row">
         <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
             <div class="card card-signin my-5">
                 <div class="card-body">
                     <h5 class="card-title text-center">Login</h5>
                     <form class="form-signin" action="../process/login.php" method="POST">
                         <div class="form-label-group">
                             <input type="email" id="email" name="email" class="form-control" placeholder="Email" required autofocus>
                             <label for="inputEmail">Email </label>
                         </div>

                         <div class="form-label-group">
                             <input type="password" id="password" name="password"  class="form-control" placeholder="Mot de Passe" required>
                             <label for="inputPassword">Mot de Passe</label>
                         </div>

                         <div class="custom-control custom-checkbox mb-3">
                             <input type="checkbox" class="custom-control-input" id="customCheck1">
                             <label class="custom-control-label" for="customCheck1">Remember password</label>
                         </div>
                         <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Se Connecter</button>
                     </form>
                 </div>
             </div>
         </div>
     </div>
 </div>
<?php include('modules/partie3.php'); ?>