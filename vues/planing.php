<?php include('modules/partie1.php'); ?>
<?php

require_once (__DIR__."/../models/Database.php");
$database = new Database();


// On crée les index associés au jour
const LUNDI     = 1;
const MARDI     = 2;
const MERCREDI  = 3;
const JEUDI     = 4;
const VENDREDI  = 5;
const SAMEDI    = 6;

// Ranger les séances par jour de la semaine en commencant le lundi 
$seances =[];
$seances[LUNDI]     =[];
$seances[MARDI]     =[];
$seances[MERCREDI]  =[];
$seances[JEUDI]     =[];
$seances[VENDREDI]  =[];
$seances[SAMEDI]    =[];

$seancesOfWeek = $database-> getSeanceByWeek(date("W"));

foreach ($seancesOfWeek as $seance){
    //On détermine le jour de la séance
   
    $indexDay = date("w", strtotime($seance->getDate()));
    // On ajoute la séance dans un tableau associé au numéro du jour de la semaine
 
    array_push($seances["$indexDay"], $seance);
}
?>

<div class="container card text-center mt-4">
    <h1 class="card-header">Planing de la Semaine</h1>
    <div class="card-body">
        <div class="row">
            <div id="lundi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>Lundi</h3>
                <?php
                foreach($seances[LUNDI] AS $seance){
                    include('modules/etiquette.php');
                }
                ?>
            </div>
            <div id="mardi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>Mardi</h3>
                <?php
                foreach($seances[MARDI] AS $seance){
                    include('modules/etiquette.php');
                }
                ?>
            </div>
            <div id="mercredi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>Mercredi</h3>
                <?php
                foreach($seances[MERCREDI] AS $seance){
                    include('modules/etiquette.php');
                } 
                ?>
            </div>
            <div id="jeudi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>Jeudi</h3>
                <?php
                foreach($seances[JEUDI] AS $seance){
                    include('modules/etiquette.php');
                } 
                ?>
            </div>
            <div id="vendredi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>Vendredi</h3>
                <?php
                foreach($seances[VENDREDI] AS $seance){
                    include('modules/etiquette.php');
                } 
                ?>
            </div>
            <div id="samedi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>Lundi</h3>
                <?php
                foreach($seances[SAMEDI] AS $seance){
                    include('modules/etiquette.php');
                } 
                ?>
            </div>
        </div>
    </div>
</div>

<?php include('modules/partie3.php'); ?>