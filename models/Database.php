<?php

include_once ('Seance.php');
include_once ('User.php');
/**
 * Classe qui fait l'interface entre la vue et la base de données
 */

class Database
{
    // faire la connexion dans des variables
    const DB_HOST       = "mariadb";
    const DB_PORT       = "3306";
    const DB_NAME       = "clublambda";
    const DB_USER       = "adminClub";
    const DB_PASSWORD   = "@!Mr8Qr36p";

    // Attribut privé qui va contenir la connexion à la BD
    private $connexion;

    // On crée la connexion dans le constructeur
    public function __construct() {
    try   {
     
    // On instancie un nouvel objet PDO
        $this->connexion = new PDO(
            "mysql:host=".self::DB_HOST.";port=".self::DB_PORT.";dbname=".self::DB_NAME.";charset=UTF8",
                          self::DB_USER,
                          self::DB_PASSWORD);
        } catch (PDOException $e)  {
             echo 'Connexion échouée :'. $e->getMessage();
         }
    
    }



    /**
     * Fonction pour créer une nouvelle seance en base de données
     * @param{Seance} seance: la seance à sauvegarder
     * @return {integer, bolean} l'id si la séance a été crée ou false sinon
     */
    public function createSeance(Seance $seance){
        // Preparation de requète SQL
        $pdoStatement = $this->connexion->prepare("
            INSERT INTO seances(titre,description,heureDebut,date,duree,nbParticipantsMax,couleur) 
            VALUES (:titre, :description, :heureDebut, :date, :duree, :nbParticipantsMax, :couleur)");
        
        //J'execute ma requète en passant les valeur de l'objet sence en valeur

        $pdoStatement->execute([
         "titre"                =>$seance->getTitre(),
         "description"          =>$seance->getDescription(),
         "heureDebut"           =>$seance->getheureDebut(),
         "date"                 =>$seance->getDate(),
         "duree"                =>$seance->getDuree(),
         "nbParticipantsMax"    =>$seance->getNbParticipantsMax(),
         "couleur"              =>$seance->getCouleur()
        ]);

        // Je récupère l'id créé si l'exécution s'est bien passé (code 000 de MYSQL)
        if($pdoStatement->errorCode()==0){
            $id = $this->connexion->lastInsertId();
            return $id;
        }else {
            return false;
        }
    }

    /**
     * Cette fonction cherche la seance dont l'id est passe en paramètre
     * et la returne
     * @param {integer} id: l'id de la seance recherchée
     * @return {Seance¦ bolean} : un objet Seance si la séance a été trouvée, false sinon
     */
    public function getSeanceById($id){
         // Preparation de requète SQL
         $pdoStatement = $this->connexion->prepare("SELECT * FROM seances WHERE id = :id");

         //J'execute ma requète en lui passant  l'id
         $pdoStatement -> execute([
             "id" => $id
         ]);

         // Je récupère le résultat
         $seance = $pdoStatement->fetchObject("Seance");
         return $seance;
    }


    /**
     * Cette fonction returne toutes les séances de la semaine
     * @param {integer} week:le numéro de la semaine recherchée
     * @return {Array} : un tableau de séance s'il y a des séances programmées pour cette semaine
     */
    public function getSeanceByWeek($week){
        // Preparation de requète SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM seances WHERE WEEKOFYEAR(date) =?
            ORDER BY date, heureDebut");
           

        //J'execute ma requète en lui passant  l'id

        $pdoStatement->execute([$week]);
        
        // Je récupère le résultat
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Seance");
        
        return $seances;
   }

   public function deleteAllSeance(){
        // Preparation de requète SQL
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seances");

        //J'execute ma requète
        $pdoStatement -> execute(); 
    }


    /**
     * Fonction pour metre à jour une seance en base de données
     * @param {integer} seance:la séance à mettre à jour
     * @return {bolean} : true si la séance est mise à jour ou false sinon
     */
    public function UpdateSeance(Seance $seance){
        // Preparation de requète SQL
        $pdoStatement = $this -> connexion->prepare("
            UPDATE seances SET 
                titre =:titre,
                description=:description,
                heureDebut=:heureDebut,
                date=:date,
                duree=:duree,
                nbParticipantsMax=:nbParticipantsMax,
                couleur=:couleur
            WHERE id=:id");
        
        //J'execute ma requète en passant les valeur de l'objet séance en valeur
        $pdoStatement->execute([
         "titre"                =>$seance->getTitre(),
         "description"          =>$seance->getDescription(),
         "heureDebut"           =>$seance->getheureDebut(),
         "date"                 =>$seance->getDate(),
         "duree"                =>$seance->getDuree(),
         "nbParticipantsMax"    =>$seance->getNbParticipantsMax(),
         "couleur"              =>$seance->getCouleur(),
         "id"                   =>$seance->getId()
        ]);

        // Returne true créé si l'exécution s'est bien passé (code 000 de MYSQL)
        if($pdoStatement->errorCode()==0){
            return true;
        }else {
            return false;
        }
    }

    /**
     * PAGE 47
     * Fonction pour suprimer une séence en base de données
     * @param {integer} id: l'id de la séance à suprimer
     * @return {bolean} true si la séance est suprimée ou false sinon
     */
     public function deleteSeance($id) {
         // Je prepare la requette pour supprimer tous les inscrits à la séances
        $pdoStatement = $this->connexion->prepare(
             "DELETE FROM inscrits WHERE id_seance = :seance"
         );
        //J'execute ma requète
        $pdoStatement -> execute([
            "seance" => $id
        ]); 
        // si ca ne s'est pas bien passé ce n'est pas la paine de continuer 
        if($pdoStatement->errorCode() !=0) {
            return false;
        }
 
        // Si les inscrits sont suprimés, je prépare la requete pour suprimer la séance
        $pdoStatement= $this->connexion->prepare(
             "DELETE FROM seances WHERE id = :seance"
         );
        //J'execute ma requète
        $pdoStatement -> execute([
            "seance" => $id
        ]); 
        
        // Return true créé si l'execution s'est bien passée (code 000 de MYSQL) 
        if($pdoStatement->errorCode() ==0) {
            return true;
        } else {
            return false;
        }          
     }

     //page 48
     public function insertParticipant($idSeance, $idUser) {
         // Je prépare la requete d'insertion
        $pdoStatement = $this->connexion->prepare(
             "INSERT INTO inscrits (id_user, id_seance) VALUES (:id_user, :id_seance)"
         );
         // J'execute ma requête
        $pdoStatement-> execute([
             "id_user"      =>$idUser,
             "id_seance"   =>$idSeance
         ]); 
         // Return true créé si l'execution s'est bien passée (code 000 de MYSQL) 
        if($pdoStatement->errorCode() ==0) {
            return true;
        } else {
            return false;
        }  
     }


     public function deleteParticipant($idSeance, $idUser) {
        // Je prépare la requete de suprimer
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits WHERE id_user=:id_user AND id_seance=:id_seance"
        );
        // J'execute ma requête
        $pdoStatement-> execute([
            "id_user"      =>$idUser,
            "id_seance"   =>$idSeance
        ]);

        // Return true créé si l'execution s'est bien passée (code 000 de MYSQL) 
       if($pdoStatement->errorCode() ==0) {
           return true;
       } else {
           return false;
       }  
    }

    /**
     * Page 51
     * Fonction pour créer une nouvelle seance en base de données
     * @param{Seance} seance: la seance à sauvegarder
     * @return {integer, bolean} l'id si la séance a été crée ou false sinon
     */
    public function createUser(User $user){
        // Preparation de requète SQL
        $pdoStatement = $this -> connexion->prepare("
            INSERT INTO users(nom,email,password,isAdmin,isActif,token) 
            VALUES (:nom,:email,:password,:isAdmin,:isActif,:token)"
        );
        //J'execute ma requète en passant les valeur de l'objet sence en valeur
        $pdoStatement->execute([
            "nom"      =>$user->getNom(),
            "email"    =>$user->getemail(),
            "password" =>$user->getpassword(),
            "isAdmin"  =>$user->isAdmin(),
            "isActif"  =>$user->isActif(),
            "token"    =>$user->gettoken()
        ]);

        // Je récupère l'id créé si l'exécution s'est bien passé (code 000 de MYSQL)
        if($pdoStatement->errorCode()==0){
            $id = $this->connexion->lastInsertId();
            return $id;
        }else {
            return false;
        }
    }

    public function deleteAllUser(){
        // Preparation de requète SQL
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM users");
    
        //J'execute ma requète
        $pdoStatement -> execute(); 
    }

    public function deleteAllInscrit(){
        // Preparation de requète SQL
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits");
        //J'execute ma requète
        $pdoStatement -> execute(); 
    }
    
    /**
     * Page 53
     * Cette fonction cherche user dont l'id est passe en paramètre
     * et le returne
     * @param {integer} id: l'id de user recherchée
     * @return {user bolean} : un objet user si la séance a été trouvée, false sinon
     */
    public function getUserById($id){
        // Preparation de requète SQL
        $pdoStatement = $this->connexion->prepare("SELECT * FROM users WHERE id = :id");

        //J'execute ma requète en lui passant  l'id
        $pdoStatement -> execute([
            "id" => $id
        ]);
        // Je récupère le résultat
        $user = $pdoStatement->fetchObject("User");
        return $user;
   }

    /**
     * Page 53
     * Active user dont l'id est passe en paramètre
     * @param {integer} id: l'id de user à activer
     * @return {user bolean} : true si l'activation a bien s'est passé, false sinon
     */
    public function activateUser($id) {
        // Je prépare ma requete  
        $pdoStatement = $this->connexion->prepare(
            "UPDATE users SET isActif =1 WHERE id = :id"
        );
        // J'execute ma requête
        $pdoStatement-> execute([
            "id"      =>$id, 
        ]);
        // Return true créé si l'execution s'est bien passée (code 000 de MYSQL) 
    if($pdoStatement->errorCode() ==0) {
        return true;
        } else {
            return false;
        }  
    }

    /**
     * Page 56
     *Verifie si un email existe déjà dans la table users
     * @param {string} email: un email utiliser pour s'inscrire
     * @return {user bolean} : true si l'email existe déjà, false sinon
     */
    public function isEmailExists($email) {
        // Je prépare ma requete  
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM users WHERE email = :email"
        );
        // J'execute ma requête
        $pdoStatement-> execute([
            "email"      =>$email
        ]);
        // Je récupère le résultat
        $nbUser = $pdoStatement->fetchColumn();
        //  si l'email n'a pas été trouvé returner false 
    if($nbUser ==0) {
        return false;
        } else {
            return true;
        }  
    }

     /**
     * Page 57
     * Cette fonction cherche user dont l'email est passe en paramètre
     * et le returne
     * @param {string} email: l'email de user recherchée
     * @return {user bolean} : un objet user si la séance a été trouvée, false sinon
     */
    public function getUserByEmail($email){
        // Preparation de requète SQL
        $pdoStatement = $this->connexion->prepare("SELECT * FROM users WHERE email = :email");

        //J'execute ma requète en lui passant  l'email
        $pdoStatement -> execute([
            "email" => $email
        ]);
        // Je récupère le résultat
        $user = $pdoStatement->fetchObject("User");
        return $user;
   }

    /**
     * Fonction qui permet de retrouver toutes les séances auxquelles est inscrit le user
     * @param {integer} id:id du user concerné
     * @return {Array} : un tableau contenant toutes les séances
     */
    public function getSeanceByUserId($idUser){
        // Preparation de requète SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT s.* FROM seances s INNER JOIN inscrits i ON s.id = i.id_seance WHERE i.id_user = :id_user");

        //J'execute ma requète en lui passant  l'id
        $pdoStatement -> execute([
            "id_user" => $idUser
        ]);
        // Je récupère le résultat
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Seance");
        return $seances;
   }

    /**
     * PAGE 90
     * Fonction qui nous permet de savoir si un utilisateur est inscrit à une séance
     * @param {integer} idUser:l'id du user 
     * @param {integer} idSeance:id de la séance
     * @return {bolean} : true si utilisateur est inscrit, false sinon
     */
    public function isInscrit($idUser, $idSeance){
        // Je prépare la ruquette d'insertion
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM inscrits WHERE id_user = :id_user and id_seance = :id_seance");

        //J'execute ma requète 
        $pdoStatement -> execute([
            "id_user"   => $idUser,
            "id_seance" => $idSeance
        ]);
        // Je récupère le résultat
        $inscrit = $pdoStatement->fetchColumn();
        // Si aucune inscription n'a été trouvée retourner false
        if($inscrit==0) {
            return false;
        }else {
            // Une inscription a été trouvée
            return true;
        }
   }

       /**
     * PAGE 90
     * Fonction qui return le nombre d'inscrits à une séance
     * @param {integer} idSeance:id de la séance
     * @return {integer} : le nombre d'inscrits
     */
    public function nombreInscrits($idSeance){
        // Je prépare la ruquette d'insertion
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM inscrits WHERE id_seance = :id_seance");

        //J'execute ma requète 
        $pdoStatement -> execute([
            "id_seance" => $idSeance
        ]);

        // Je récupère le résultat
        $nbInscrits = $pdoStatement->fetchColumn();
       
        // Une inscription a été trouvée
        return $nbInscrits;
   }



}
?>