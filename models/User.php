<?php
/**
 * Classe Users
 */

class User {
    // Les attributs de la classe sont les mêmes que les noms des 
    // colonnes dans la base de données 
    private $id;
    private $nom;
    private $email;
    private $password;
    private $isAdmin;
    private $isActif;
    private $token;


    // On a besoin de constructeur 
    public function __construct() { }

    public static function createUser ($nom, $email, $password, $isAdmin, $isActif, $token){
    $user= new self();
    $user->setNom($nom);;
    $user->setEmail($email);
    $user->setPassword($password);
    $user->setIsAdmin($isAdmin);
    $user->setIsActif($isActif);
    $user->setToken($token);
    return $user;

    }

     // Getters
    public function getId(){ return $this->id; }
    public function getNom(){ return $this->nom; }
    public function getEmail(){ return $this->email; } 
    public function getpassword(){ return $this->password; } 
    public function isAdmin(){ return $this->isAdmin; } 
    public function isActif(){ return $this->isActif; } 
    public function gettoken(){ return $this->token; } 

    // Setters
    public function setNom($nom){ $this->nom = $nom; }
    public function setEmail($email){ $this->email = $email; }
    public function setPassword($password){ $this->password = $password; }
    public function setIsAdmin($isAdmin){ $this->isAdmin = $isAdmin; }
    public function setIsActif($isActif){ $this->isActif = $isActif; }
    public function setToken($token){ $this->token = $token; }
}
