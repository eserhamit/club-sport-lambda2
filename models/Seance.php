<?php
/**
 * Classe Seance
 */


class Seance {
    // Les attributs de la classe sont les mêmes que les noms des 
    // colonnes dans la base de données 
    private $id;
    private $titre;
    private $description;
    private $heureDebut;
    private $date;
    private $duree;
    private $nbParticipantsMax;
    private $couleur;

     

    // On a pas besoin de constructeur 
    public function __construct() { }

    public static function createSeance ($titre,$description,$heureDebut,$date,$duree,$nbParticipantsMax,$couleur){
        $seance = new self;
        $seance->setTitre($titre);
        $seance->setDescription($description);
        $seance->setheureDebut($heureDebut);
        $seance->setDate($date);
        $seance->setDuree($duree);
        $seance->setNbParticipantsMax($nbParticipantsMax);
        $seance->setcouleur($couleur);
        return $seance;

    }

    // On implémente les getters et les setters pour chaque attribut
    // Cela va servir à lire et modifier les valeurs des attributs
    public function getId(){ return $this->id; }
    public function getTitre(){ return $this->titre; }
    public function getDescription(){ return $this->description; } 
    public function getheureDebut(){ return $this->heureDebut; }  
    public function getDate(){ return $this->date; } 
    public function getDuree(){ return $this->duree; } 
    public function getNbParticipantsMax(){ return $this->nbParticipantsMax; } 
    public function getCouleur(){ return $this->couleur; } 

    // Pas besoin de setter pour l'id car il est géré par la BD
    public function setId($id){ $this->id = $id; }
    public function setTitre($titre){ $this->titre = $titre; } 
    public function setDescription($description){ $this->description = $description; }
    public function setheureDebut($heureDebut){ $this->heureDebut = $heureDebut; } 
    public function setDate($date){ $this->date = $date; }
    public function setDuree($duree){ $this->duree = $duree; }
    public function setNbParticipantsMax($nbParticipantsMax){ $this->nbParticipantsMax = $nbParticipantsMax; }
    public function setCouleur($couleur){ $this->couleur = $couleur; }
}
